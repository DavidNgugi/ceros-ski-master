// Ensure compatibility across browsers
var requestAnimationFrame = (function(){
    return  window.requestAnimationFrame       ||
            window.webkitRequestAnimationFrame ||
            window.mozRequestAnimationFrame    ||
            window.oRequestAnimationFrame      ||
            window.msRequestAnimationFrame     ||
            function(callback, element){
              window.setTimeout(callback, 1000 / 60);
            };
})();

import Lockr from "Lockr";
import Game from "./game.js";
import Skier from "./skier.js";
import $ from 'jquery';
import {Howl, Howler} from 'howler';

$(document).ready(function() {

    // Setup global variables
    var game = new Game(), skier = new Skier();

    // Setup the new Howl.
    const sound = new Howl({
        loop: true,
        src: ['../sound/WildWaters.mp3']
    });

    var then = Date.now(), delta = 0;

    // Play Sound using Howler.js
    var playSound = function() {
        sound.play();
        // Change global volume.
        Howler.volume(2.0);  
    };

    // Pause sound
    var pauseSound = function(){
        sound.pause();
    };

    // Reset game
    var reset = function(){
        Lockr.rm('game_data');
        game.reset();
        skier.reset();
    };

    /**
     * Stops the game, clears the canvas and saves the scrore
     */
    var gameOver = function(){
        sound.stop();
        // store highscore
        var score = Math.round(game.score);
        Lockr.sadd('highscores', score);
        // reset everything
        reset();
        $('.pause').remove();
        $('canvas').remove();
        // show Game Over screen
        $('#score').html(score + " metres");
        $('.game-start-menu, .game-pause-menu').hide();
        $('.game-ui, .game-over-screen').show();
    };

    // Set game state to paused and show pause menu
    var pauseAndShowPausedGameUI = function() {

        if(game.currentState !== 'paused')
            game.setState('paused', true);

        pauseSound();
        $('.canvas-container').hide();
        $('.game-ui, .game-pause-menu').show();
    };

    // check game state before initializing anything else
    if(game.currentState === 'started'){
        $('.game-ui, .game-start-menu').show();
    }else if(game.currentState === 'paused'){
        pauseAndShowPausedGameUI();
    }else{
        gameOver();
    }

    /**
     * The GameLoop where we draw our game objects, update them and check for collisions
     * @return void
     */
    var gameLoop = function() {

            game.ctx.save();

            // Retina support
            game.ctx.scale(window.devicePixelRatio, window.devicePixelRatio);

            game.clearCanvas();

            skier.move(game);

            game.placeNewObstacle(skier);

            game.checkIfSkierHitObstacle(skier);

            game.drawScore();

            game.drawSkier(skier);

            game.drawObstacles(skier);

            game.ctx.restore();

            if( (skier.hasCollided === false) && (game.currentState !== 'paused') ){
                requestAnimationFrame(gameLoop);
            }else if(game.currentState === 'paused'){
                pauseAndShowPausedGameUI();
            }else{
                gameOver();
            }
    };

    /**
     * Checks for keyboard onKeyDown events
     * @param {int} update 
     * @return void
     */
    var setupKeyhandler = function() {

        $(document).keydown(function(event) {
            // var now = Date.now();
            // delta = (now - then)/10000;
            skier.isMoving = true;

            event.preventDefault();

            switch(event.which) {
                case 37: // left
                    if(skier.direction === 0)
                        skier.direction = 1;
                    else
                        skier.direction = 2;
                    
                     skier.mapX += skier.speed;

                    break;
                case 39: // right
                    if(skier.direction === 0)
                        skier.direction = 5;
                    else
                        skier.direction = 4;

                    skier.mapX += skier.speed;

                    break;
                case 38: // up
                    skier.mapY -= skier.speed;

                    break;
                case 40: // down
                    skier.direction = 3;
                    skier.mapY += skier.speed;
                    break;
            }
        });
    };

    /**
     * Start a new game
     * @param {float} then 
     */
    var initGame = function() {

        console.log('Initializing the game...');

        setCanvas();

        console.log("current state: " + game.currentState);

        game.loadAssets().then(function() {
            game.placeInitialObstacles();
            requestAnimationFrame(gameLoop);
        });

        playSound();

        setupKeyhandler(delta/1000);

    };

    /**
     * Creates a new canvas element and appends to the body and creates the game context
     * @return void
     */
    var setCanvas = function(){
        var canvas = $('<canvas></canvas>')
            .attr('width', game.width * window.devicePixelRatio)
            .attr('height', game.height * window.devicePixelRatio)
            .css({
                width: game.width + 'px',
                height: game.height + 'px',
                'z-index': '1 !important'
            });

        var pause_button = $("<button></button>")
            .attr('class', 'pause button')
            .attr('width', 200 * window.devicePixelRatio)
            .attr('height', 100 * window.devicePixelRatio)
            .css({
                'position': 'absolute !important',
                'z-index': '2 !important'
            })
            .html('Pause Game');

        $('body .canvas-container').append(canvas);
        $('body .canvas-container').append(pause_button);

        game.createContext(canvas);
    };

    // Event Listeners

    // Play/New Game
    $('.play').on('click', function(e){
        $('.game-ui').hide();
        reset();
        initGame();
    });

    // Resume Game
    $('#resume').on('click', function(e){
        $('.game-ui').hide();
        // var then = Date.now();
        // resumeGame(then);
    });

    // Pause Game
    $('.pause').on('click', function(e){
        e.preventDefault();
        // save game to localStorage
        var score = Math.round(game.score);
        Lockr.sadd('highscores', score);
        // store game state
        Lockr.set('game_data', {'game': game, 'skier': skier});
        // show menu
        pauseAndShowPausedGameUI();
    });

    // Quit game
    $("#quit").on('click', function(e){
        reset();
        $('.game-pause-menu').hide();
        $('.game-start-menu').show();
    });

    // Go Back to Main Menu
    $('.back').on('click', function(e){
        $('.highscore-screen,.game-over-screen').hide();
        $('.game-start-menu').show();
    });

    // View High Scores
    $('#highscore').on('click', function(e){
        $('.game-start-menu').hide();
        var highscores = Lockr.get('highscores');
        if(highscores !== undefined){
            $('#highscores li').remove();
            highscores.sort(((a,b) => { return b-a;})).forEach((highscore) => {
                $('#highscores').append('<li> '+highscore+' metres</li>');
            });
        }
        $('.highscore-screen').show();
    });

});