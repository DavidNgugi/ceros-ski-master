import AssetStore from "./assetStore.js";

/**
 * Holds game state, settings and logic
 */
export default class Game {
    
    constructor(){
        this.ctx = null;
        this.assets = new AssetStore();
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.loadedAssets = {};
        this.obstacleTypes = ['tree', 'treeCluster', 'rock1', 'rock2'];
        this.obstacles = [];
        this.state = { 'started': false, 'paused': false, 'resumed': false, 'gameOver': false};
        this.score= 0;
    }

    /**
     * Resets properties to default
     * @return void
     */
    reset(){
        this.width = window.innerWidth;
        this.height = window.innerHeight;
        this.loadedAssets = {};
        this.obstacleTypes = ['tree', 'treeCluster', 'rock1', 'rock2'];
        this.obstacles = [];
        this.state = { 'started': false, 'paused': false, 'resumed': false, 'gameOver': false};
        this.score= 0;
    }

    /**
     * Asynchronously Load each asset from Asset Store 
     * @return {Promise}
     */
    loadAssets() {
        var assetPromises = [];

        var context = this;
        _.each(this.assets.store, function(asset, assetName) {

            var assetImage = new Image();
            var assetDeferred = new $.Deferred();

            var that = context;
            assetImage.onload = function() {
                assetImage.width /= 2;
                assetImage.height /= 2;
                that.loadedAssets[assetName] = assetImage;
                assetDeferred.resolve();
            };
            assetImage.src = asset;

            assetPromises.push(assetDeferred.promise());
        });

        return $.when.apply($, assetPromises);
    }

    /**
     * Randomly places initial obstacles on the map as game loads
     * @return void
     */
    placeInitialObstacles(){
        var numberObstacles = Math.ceil(_.random(5, 7) * (this.width / 800) * (this.height / 500));

        var minX = -50;
        var maxX = this.width + 50;
        var minY = this.height / 2 + 100;
        var maxY = this.height + 50;
        var i = 0;

        for(i = 0; i < numberObstacles; i++) {
            this.placeRandomObstacle(minX, maxX, minY, maxY);
        }

        var that = this;
        this.obstacles = _.sortBy(this.obstacles, function(obstacle) {
            var obstacleImage = that.loadedAssets[obstacle.type];
            return obstacle.y + obstacleImage.height;
        });

    }

    /**
     * Calculates positions that are empty/open on the map
     * @param {int} minX 
     * @param {int} maxX 
     * @param {int} minY 
     * @param {itn} maxY 
     * @return {Object} Position of the object
     */
    calculateOpenPosition(minX, maxX, minY, maxY) {
        try{
            // console.log("minX: " + minX + ", maxX: " + maxX + ", minY:" + minY +", maxY: "+ maxY);
                var x = _.random(minX, maxX);
                var y = _.random(minY, maxY);
            
            var foundCollision = _.find(this.obstacles, function(obstacle) {
                return x > (obstacle.x - 50) && x < (obstacle.x + 50) && y > (obstacle.y - 50) && y < (obstacle.y + 50);
            });

            if(foundCollision) {
                return this.calculateOpenPosition(minX, maxX, minY, maxY);
            }
            else {
                return {
                    x: x,
                    y: y
                }
            }
        }catch(e){
            // console.log(e);
            return { x: 0, y: 0};
        }
    }

    /**
     * Finds Open positions and places a new obstacle in those positions
     * @param {int} minX 
     * @param {int} maxX 
     * @param {int} minY 
     * @param {itn} maxY 
     * @return void
     */
    placeRandomObstacle(minX, maxX, minY, maxY) {
        var obstacleIndex = _.random(0, this.obstacleTypes.length - 1);

        var position = this.calculateOpenPosition(minX, maxX, minY, maxY);

        this.obstacles.push({
            type : this.obstacleTypes[obstacleIndex],
            x : position.x,
            y : position.y
        })
    }

    /**
     * Place a single obstacle based on skier direction of movement
     * Endless snow with obstacles illusion
     * @param {Object} skier
     * @return void
     */
    placeNewObstacle(skier) {
        var shouldPlaceObstacle = _.random(1, 8);
        if(shouldPlaceObstacle !== 8) {
            return;
        }

        var leftEdge = skier.mapX;
        var rightEdge = skier.mapX + this.width;
        var topEdge = skier.mapY;
        var bottomEdge = skier.mapY + this.height;

        switch(skier.direction) {
            case 1: // left
                this.placeRandomObstacle(leftEdge - 50, leftEdge, topEdge, bottomEdge);
                break;
            case 2: // left down
                this.placeRandomObstacle(leftEdge - 50, leftEdge, topEdge, bottomEdge);
                this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + 50);
                break;
            case 3: // down
                this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + 50);
                break;
            case 4: // right down
                this.placeRandomObstacle(rightEdge, rightEdge + 50, topEdge, bottomEdge);
                this.placeRandomObstacle(leftEdge, rightEdge, bottomEdge, bottomEdge + 50);
                break;
            case 5: // right
                this.placeRandomObstacle(rightEdge, rightEdge + 50, topEdge, bottomEdge);
                break;
            case 6: // up
                this.placeRandomObstacle(leftEdge, rightEdge, topEdge - 50, topEdge);
                break;
        }
    }

    /**
     * Draw a score board on top left
     * @return void
     */
    drawScore() {
        this.ctx.font = "30px Arial";
        this.ctx.fillStyle = "#0095DD";
        this.ctx.fillText("Score: "+ Math.round(this.score) + " metres", 10, 35);
    }

    /**
     * Draws the skier object
     * @param {Object} skier 
     * @return void
     */
    drawSkier(skier) {
        var skierAssetName = skier.asset;
        var skierImage = this.loadedAssets[skierAssetName];
        var x = (this.width - skierImage.width) / 2;
        var y = (this.height - skierImage.height) / 2;

        this.ctx.drawImage(skierImage, x, y, skierImage.width, skierImage.height);
    }

    /**
     * Draws obstacles
     * @param {Object} skier 
     * @return void
     */
    drawObstacles(skier) {
        var newObstacles = [];

        var that = this;
        _.each(this.obstacles, function(obstacle) {
            var obstacleImage = that.loadedAssets[obstacle.type];
            var x = obstacle.x - skier.mapX - obstacleImage.width / 2;
            var y = obstacle.y - skier.mapY - obstacleImage.height / 2;

            if(x < -100 || x > that.width + 50 || y < -100 || y > that.height + 50) {
                return;
            }

            that.ctx.drawImage(obstacleImage, x, y, obstacleImage.width, obstacleImage.height);

            newObstacles.push(obstacle);
        });

        this.obstacles = newObstacles;
    }

     /**
     * Checks if 2 rectangular objects have intersected
     * @param {Object} r1 
     * @param {Object} r2 
     * @return {boolean}
     */
    intersectRect(r1, r2) {
        return !(r2.left > r1.right ||
            r2.right < r1.left ||
            r2.top > r1.bottom ||
            r2.bottom < r1.top);
    }

    /**
     * Checks for collisions between the skier and obstacles
     * @param {Object} skier
     * @return void
     */
    checkIfSkierHitObstacle(skier) {
        var skierAssetName = skier.asset;
        var skierImage = this.loadedAssets[skierAssetName];
        var skierRect = {
            left: skier.mapX + this.width / 2,
            right: skier.mapX + skierImage.width + this.width / 2,
            top: skier.mapY + skierImage.height - 5 + this.height / 2,
            bottom: skier.mapY + skierImage.height + this.height / 2
        };

        var that = this;
        var collision = _.find(this.obstacles, function(obstacle) {
            var obstacleImage = that.loadedAssets[obstacle.type];
            var obstacleRect = {
                left: obstacle.x,
                right: obstacle.x + obstacleImage.width,
                top: obstacle.y + obstacleImage.height - 5,
                bottom: obstacle.y + obstacleImage.height
            };

            return that.intersectRect(skierRect, obstacleRect);
        });

        if(collision) {
            skier.direction = 0;
            skier.isMoving = false;
            skier.hasCollided = true;
        }else if(skier.isMoving){
            this.score += 0.5;
        }
    }

    /**
     * Loads assets, places the obstacles and runs the Game Loop
     * @param {fn} gameLoop 
     * @return void
     */
    // loadNewGame(gameLoop) {
    //     // clear previous game data

    //     // load new game
    //     var that = this;
    //     this.loadAssets().then(function() {
    //         that.placeInitialObstacles();
    //         requestAnimationFrame(gameLoop);
    //     });
    // }

    // resumeGame(gameLoop) {
        // var that = this;
    //     this.loadSavedAssets().then(function() {
    //         that.placeSavedObstacles();
    //         requestAnimationFrame(gameLoop);
    //     });
    // }

    /**
     * Sets the value of specified state variable
     * @param {string} key 
     * @param {boolean} value 
     */
    setState(key, value){
        this.state[key] = value;
    }

    /**
     * Returns the value of a given state
     * @param {string} key 
     * @return {boolean}
     */
    getState(key){
        return this.state[key];
    }

    /**
     * Gets the currently true state
     * @return {string}
     */
    get currentState(){
        var currentState;
        for(var state in this.state){
            if(this.state[state])
                currentState = state;
        }
        
        return (currentState !== undefined) ? currentState : 'started';
    }

    /**
     * Resets all state properties to their default false
     * @return void
     */
    resetState(){
        for(var state in this.state)
            this.state[state] = false;
    }

    /**
     * Creates the Canvas Context
     * @param {Canvas} canvas 
     * @return void
     */
    createContext(canvas) {
        this.ctx = canvas[0].getContext('2d');
    };

    /**
     * Clears everything drawn on the canvas
     * @return void
     */
    clearCanvas() {
        this.ctx.clearRect(0, 0, this.width, this.height);
    }

   
}