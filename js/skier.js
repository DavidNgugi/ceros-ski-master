/**
 * The player component
 */
export default class Skier {

    constructor(){
        this.direction = 5;
        this.mapX = 0;
        this.mapY = 0;
        this.speed = 8;
        this.isMoving = false;
        this.hasCollided = false;
        this.assets = ['skierCrash', 'skierLeft', 'skierLeftDown', 'skierDown', 'skierRightDown', 'skierRight'];
    }

    /**
     * Resets properties to default
     * @return void
     */
    reset(){
        this.direction = 5;
        this.mapX = 0;
        this.mapY = 0;
        this.speed = 8;
        this.isMoving = false;
        this.hasCollided = false;
    }

    /**
     * Gets position vector
     * @return {object}
     */
    get position(){
        return { x : this.mapX, y : this.mapY };
    }

    /**
     * Gets specific asset based on the direction this is facing
     * @return {string}
     */
    get asset(){        
        return this.assets[this.direction];
    }

     /**
     * Moves the skier according to the direction and places new objects on the map
     * @return void
     */
    move() {
        var dt = 1;
        switch(this.direction) {
            case 2:
                this.mapX = this.lerp(this.mapX, (this.mapX - Math.round(this.speed / 1.4142)), dt);
                this.mapY = this.lerp(this.mapY, (this.mapY + Math.round(this.speed / 1.4142)), dt);
                break;
            case 3:
                this.mapY = this.lerp(this.mapY, (this.mapY + this.speed), dt);
                break;
            case 4:
                this.mapX = this.lerp(this.mapX, (this.mapX + this.speed / 1.4142), dt);
                this.mapY = this.lerp(this.mapY, (this.mapY + this.speed / 1.4142), dt);
                break;

        }
    }

    /**
     * Linerar Interpolation
     * @param {int} current 
     * @param {int} goal 
     * @param {int} dt 
     */
    lerp(current, goal, dt){
        return current + (goal-current) * dt;
    }

}